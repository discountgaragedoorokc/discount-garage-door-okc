Locally owned and operated since 2001, Discount Garage Door is a Family owned company that is dedicated to quality, affordability and customer service. Specializing in garage doors, garage door repair, new garage door installations, and garage door openers.

Address: 400 S Vermont Ave, Ste 125, Oklahoma, OK 73108, USA

Phone: 405-525-3667

Website: https://okdiscountgaragedoor.com
